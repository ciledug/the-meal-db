# README #

This application shows meals information taken from [https://www.themealdb.com/api.php](https://www.themealdb.com/api.php). Here are some screenshots from the application:

![Splash screen](https://farmasikita.my.id/img/test/themealdb.com_01.jpg)&ensp;![Category list](https://farmasikita.my.id/img/test/themealdb.com_02.jpg)&ensp;![Meal list](https://farmasikita.my.id/img/test/themealdb.com_03.jpg)&ensp;![Meal detail](https://farmasikita.my.id/img/test/themealdb.com_04.jpg)

### Implements ###

The application implements the following:

* [Java/JDK 8](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)
* [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)
* [Data Binding](https://developer.android.com/topic/libraries/data-binding)
* [MVVM](https://developer.android.com/jetpack/guide)
* [ButterKnife](https://jakewharton.github.io/butterknife)
* [Glide](https://github.com/bumptech/glide)
* [Retrofit](https://square.github.io.retrofit/)

### Disclaimer ###

All writings and image assets are taken from [https://www.themealdb.com/](https://www.themealdb.com).

All codes in this repository are simply meant for testing only.

DO NOT USE THESE CODES FOR PRODUCTION USES!