package id.my.farmasikita.themealdb.ui.meals;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import id.my.farmasikita.themealdb.R;
import id.my.farmasikita.themealdb.databinding.ItemMealBinding;
import id.my.farmasikita.themealdb.models.CategoryList;
import id.my.farmasikita.themealdb.models.Meal;
import id.my.farmasikita.themealdb.utils.Tools;

public class MealsAdapter extends RecyclerView.Adapter<MealsAdapter.MealsAdapterViewHolder> {

    private final String TAG = getClass().getSimpleName();

    public interface MealsAdapterListener {
        void onMealClicked(String idMeal);
    }

    private Context mContext;
    private List<Meal> mCategoryList = new ArrayList<>();
    private MealsAdapterListener mListener;

    public MealsAdapter(Context context, CategoryList categoryList, MealsAdapterListener listener) {
        mContext = context;
        mCategoryList = categoryList.getCategories();
        mListener = listener;
    }

    @NonNull
    @Override
    public MealsAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemMealBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_meal,
                parent,
                false
        );
        return new MealsAdapter.MealsAdapterViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MealsAdapterViewHolder holder, int position) {
        if (position == 0) {
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) holder.itemBinding.getRoot().getLayoutParams();
            lp.setMargins(lp.leftMargin, lp.bottomMargin, lp.rightMargin, lp.bottomMargin);
            holder.itemBinding.getRoot().setLayoutParams(lp);
        }
        holder.itemBinding.setMeal(mCategoryList.get(position));
        holder.itemBinding.cvMealBodyContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onMealClicked(mCategoryList.get(position).getIdMeal());
                }
            }
        });

        Tools.setImage(mContext, holder.itemBinding.ivMealIcon, mCategoryList.get(position).getStrMealIcon());
    }

    @Override
    public int getItemCount() {
        return mCategoryList.size();
    }

    public void addItems(List<Meal> newItems) {
        int currentPos = mCategoryList.size() - 1;
        mCategoryList.addAll(newItems);
        notifyItemInserted(currentPos);
    }

    public class MealsAdapterViewHolder extends RecyclerView.ViewHolder {

        ItemMealBinding itemBinding;

        public MealsAdapterViewHolder(ItemMealBinding binding) {
            super(binding.getRoot());
            itemBinding = binding;
        }
    }
}
