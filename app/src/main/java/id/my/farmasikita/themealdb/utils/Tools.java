package id.my.farmasikita.themealdb.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;

import id.my.farmasikita.themealdb.ui.category.CategoryListActivity;

public class Tools {

    public static void gotoMainActivity(AppCompatActivity activity) {
        Intent i = new Intent(activity, CategoryListActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(i);
        activity.finish();
    }

    public static void setupToolbarNavigation(AppCompatActivity activity, Toolbar toolbar, View.OnClickListener onClickListener) {
        activity.setSupportActionBar(toolbar);
        toolbar.setElevation(8.0f);

        if (onClickListener != null) {
            toolbar.setNavigationOnClickListener(onClickListener);
        }
    }

    public static void setImage(Context context, ImageView iv, String url) {
        Glide.with(context)
                .load(url)
                .centerCrop()
                .into(iv);
    }

    public static void setImage(Context context, ImageView iv, Drawable drawable) {
        Glide.with(context)
                .load(drawable)
                .centerCrop()
                .into(iv);
    }

    public static void hideKeyboard(InputMethodManager imm, View view) {
        imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
}
