package id.my.farmasikita.themealdb.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import id.my.farmasikita.themealdb.models.CategoryList;
import id.my.farmasikita.themealdb.models.Meal;
import id.my.farmasikita.themealdb.repositories.TheMealDBAPIService;
import id.my.farmasikita.themealdb.repositories.deserializers.CategoryDeserializer;
import id.my.farmasikita.themealdb.repositories.deserializers.MealDeserializer;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class TheMealDBRetrofitUtil {

    private static boolean isRequestNewToken = false;
    private static OkHttpClient client;

    public static TheMealDBAPIService create() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(loggingInterceptor);

        client = builder
                .addInterceptor(loggingInterceptor)
                .connectTimeout(TheMealDBAPIService.TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TheMealDBAPIService.READ_TIMEOUT, TimeUnit.SECONDS)
                .build();

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(CategoryList.class, new CategoryDeserializer())
                .registerTypeAdapter(Meal.class, new MealDeserializer())
                .serializeNulls()
                .create();

        String url = TheMealDBAPIService.developmentURL;

        return new Retrofit.Builder()
                .client(client)
                .baseUrl(url)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(TheMealDBAPIService.class);
    }
}
