package id.my.farmasikita.themealdb.ui.detail;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import id.my.farmasikita.themealdb.models.CategoryList;
import id.my.farmasikita.themealdb.repositories.DataRepository;

public class DetailViewModel extends ViewModel {

    private final String TAG = getClass().getSimpleName();

    private MutableLiveData<CategoryList> mCategories;
    private DataRepository mDataRepository;

    public DetailViewModel() {
        mDataRepository = DataRepository.getInstance();
    }

    public LiveData<CategoryList> requestDetail(String idMeal) {
        if (mCategories == null) {
            mCategories = mDataRepository.requestMealDetail(idMeal);
        }
        return mCategories;
    }

}