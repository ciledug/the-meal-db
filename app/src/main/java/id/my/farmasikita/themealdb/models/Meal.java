package id.my.farmasikita.themealdb.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Meal implements Serializable {

    @SerializedName("strCategory")
    private String mStrCategory = "";

    @SerializedName("strMeal")
    private String mStrMeal = "";

    @SerializedName("strMealThumb")
    private String mStrMealThumb = "";
    
    @SerializedName("idMeal")
    private String mIdMeal = "";

    @SerializedName("strArea")
    private String mStrArea;

    @SerializedName("strInstructions")
    private String mStrInstructions = "";

    @SerializedName("strTags")
    private String mStrTags = "";

    @SerializedName("strYoutube")
    private String mStrYoutube = "";

    @SerializedName("strSource")
    private String mStrSource = "";

    @SerializedName("ingredients")
    private List<Ingredient> mIngredients = new ArrayList<>();


    public Meal() {
    }

    public String getStrCategory() {
        return mStrCategory;
    }

    public void setStrCategory(String mStrCategory) {
        this.mStrCategory = mStrCategory;
    }

    public String getStrMeal() {
        return mStrMeal;
    }

    public void setStrMeal(String mStrMeal) {
        this.mStrMeal = mStrMeal;
    }

    public String getStrMealThumb() {
        return mStrMealThumb;
    }

    public void setStrMealThumb(String mStrMealThumb) {
        this.mStrMealThumb = mStrMealThumb;
    }

    public String getStrMealIcon() {
        return mStrMealThumb.concat("/preview");
    }

    public String getIdMeal() {
        return mIdMeal;
    }

    public void setIdMeal(String mIdMeal) {
        this.mIdMeal = mIdMeal;
    }

    public String getStrArea() {
        return mStrArea;
    }

    public void setStrArea(String mStrArea) {
        this.mStrArea = mStrArea;
    }

    public String getStrInstructions() {
        return mStrInstructions;
    }

    public void setStrInstructions(String mStrInstructions) {
        this.mStrInstructions = mStrInstructions;
    }

    public String getStrTags() {
        return mStrTags;
    }

    public void setStrTags(String mStrTags) {
        this.mStrTags = mStrTags;
    }

    public String getStrYoutube() {
        return mStrYoutube;
    }

    public void setStrYoutube(String mStrYoutube) {
        this.mStrYoutube = mStrYoutube;
    }

    public String getStrSource() {
        return mStrSource;
    }

    public void setStrSource(String mStrSource) {
        this.mStrSource = mStrSource;
    }

    public List<Ingredient> getIngredients() {
        return mIngredients;
    }

    public void setIngredients(List<Ingredient> mIngredients) {
        this.mIngredients = mIngredients;
    }

}
