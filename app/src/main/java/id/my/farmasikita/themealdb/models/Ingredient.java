package id.my.farmasikita.themealdb.models;

import java.io.Serializable;

public class Ingredient implements Serializable {

    private String mName = "";
    private String mMeasure = "";

    public Ingredient() {
    }

    public Ingredient(String mName, String mMeasure) {
        this.mName = mName;
        this.mMeasure = mMeasure;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getMeasure() {
        return mMeasure;
    }

    public void setMeasure(String mMeasure) {
        this.mMeasure = mMeasure;
    }
}
