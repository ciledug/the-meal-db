package id.my.farmasikita.themealdb.ui.meals;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.my.farmasikita.themealdb.R;
import id.my.farmasikita.themealdb.fragments.ProgressDialogFragment;
import id.my.farmasikita.themealdb.models.CategoryList;
import id.my.farmasikita.themealdb.ui.detail.DetailActivity;
import id.my.farmasikita.themealdb.utils.AppConstants;
import id.my.farmasikita.themealdb.utils.Tools;

public class MealsListActivity extends AppCompatActivity {

    private final String TAG = getClass().getSimpleName();

    private MealsAdapter mAdapter;
    private MealsViewModel mViewModel;
    private MutableLiveData<CategoryList> mCategoryList = new MutableLiveData<>();

    private String mSelectedCategory = "";
    private ProgressDialogFragment mProgressDialog;

    private MealsAdapter.MealsAdapterListener mCategoryAdapterListener = new MealsAdapter.MealsAdapterListener() {
        @Override
        public void onMealClicked(String idMeal) {
            gotoMealDetail(idMeal);
        }
    };

    @BindView(R.id.tbToolbar)
    Toolbar tbToolbar;

    @BindView(R.id.rvCategoryListContainer)
    RecyclerView rvCategoryListContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);
        ButterKnife.bind(this);

        mViewModel = new ViewModelProvider(this).get(MealsViewModel.class);
        mProgressDialog = ProgressDialogFragment.newInstance();

        if (getIntent().hasExtra(AppConstants.EXTRA_BUNDLE)) {
            Bundle b = getIntent().getBundleExtra(AppConstants.EXTRA_BUNDLE);
            if (b != null) {
                mSelectedCategory = b.getString(AppConstants.EXTRA_SELECTED_CATEGORY);
            }
        }

        initScreen();
        requestMealsList();
    }

    private void initScreen() {
        Tools.setupToolbarNavigation(this, tbToolbar, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        rvCategoryListContainer.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        );
    }

    private void requestMealsList() {
        mProgressDialog.show(getSupportFragmentManager(), AppConstants.TAG_PROGRESS);

        mViewModel.requestMeals(mSelectedCategory).observe(this, new Observer<CategoryList>() {
            @Override
            public void onChanged(CategoryList categoryList) {
                if (mAdapter == null) {
                    mAdapter = new MealsAdapter(getApplicationContext(), mViewModel.requestMeals(mSelectedCategory).getValue(), mCategoryAdapterListener);
                    rvCategoryListContainer.setAdapter(mAdapter);
                }

                mCategoryList.setValue(categoryList);
                mAdapter.addItems(mCategoryList.getValue().getCategories());
                mProgressDialog.dismiss();
            }
        });
    }

    public void gotoMealDetail(String idMeal) {
        Bundle b = new Bundle();
        b.putSerializable(AppConstants.EXTRA_SELECTED_MEAL, idMeal);

        Intent i = new Intent(this, DetailActivity.class);
        i.putExtra(AppConstants.EXTRA_BUNDLE, b);
        startActivity(i);
    }
}
