package id.my.farmasikita.themealdb.ui.category;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import id.my.farmasikita.themealdb.R;
import id.my.farmasikita.themealdb.databinding.ItemCategoryBinding;
import id.my.farmasikita.themealdb.models.CategoryList;
import id.my.farmasikita.themealdb.models.Meal;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryAdapterViewHolder> {

    private final String TAG = getClass().getSimpleName();

    public interface CategoryAdapterListener {
        void onCategoryClicked(String category);
    }

    private Context mContext;
    private List<Meal> mCategoryList = new ArrayList<>();
    private CategoryAdapterListener mListener;

    public CategoryAdapter(Context context, CategoryList categoryList, CategoryAdapterListener listener) {
        mContext = context;
        mCategoryList = categoryList.getCategories();
        mListener = listener;
    }

    @NonNull
    @Override
    public CategoryAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemCategoryBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_category,
                parent,
                false
        );
        return new CategoryAdapter.CategoryAdapterViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryAdapterViewHolder holder, int position) {
        if (position == 0) {
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) holder.itemBinding.getRoot().getLayoutParams();
            lp.setMargins(lp.leftMargin, lp.bottomMargin, lp.rightMargin, lp.bottomMargin);
            holder.itemBinding.getRoot().setLayoutParams(lp);
        }
        holder.itemBinding.setCategoryName(mCategoryList.get(position).getStrCategory());
        holder.itemBinding.cvItemCategoryContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onCategoryClicked(mCategoryList.get(position).getStrCategory());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCategoryList.size();
    }

    public void addItems(List<Meal> newItems) {
        int currentPos = mCategoryList.size() - 1;
        mCategoryList.addAll(newItems);
        notifyItemInserted(currentPos);
    }

    public class CategoryAdapterViewHolder extends RecyclerView.ViewHolder {

        ItemCategoryBinding itemBinding;

        public CategoryAdapterViewHolder(ItemCategoryBinding binding) {
            super(binding.getRoot());
            itemBinding = binding;
        }
    }
}
