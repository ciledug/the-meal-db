package id.my.farmasikita.themealdb.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CategoryList implements Serializable {

    @SerializedName("meals")
    private List<Meal> mMeals = new ArrayList<>();

    public CategoryList() {
    }

    public List<Meal> getCategories() {
        return mMeals;
    }

    public void setCategories(List<Meal> mMeals) {
        this.mMeals = mMeals;
    }
}