package id.my.farmasikita.themealdb.repositories.deserializers;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import id.my.farmasikita.themealdb.models.CategoryList;

public class CategoryDeserializer implements JsonDeserializer<CategoryList> {
    @Override
    public CategoryList deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject categories = json.getAsJsonObject();
        return new Gson().fromJson(categories, CategoryList.class );
    }
}
