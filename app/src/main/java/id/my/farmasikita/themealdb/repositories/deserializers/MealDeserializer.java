package id.my.farmasikita.themealdb.repositories.deserializers;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import id.my.farmasikita.themealdb.models.Ingredient;
import id.my.farmasikita.themealdb.models.Meal;

public class MealDeserializer implements JsonDeserializer<Meal> {
    @Override
    public Meal deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject meal = json.getAsJsonObject();
        List<Ingredient> tempIngredients = new ArrayList<>();

        if (meal.get("strIngredient1") != null) {
            tempIngredients.add(new Ingredient(meal.get("strIngredient1").getAsString(), meal.get("strMeasure1").getAsString()));
        }

        if (meal.get("strIngredient2") != null) {
            tempIngredients.add(new Ingredient(meal.get("strIngredient2").getAsString(), meal.get("strMeasure2").getAsString()));
        }

        if (meal.get("strIngredient3") != null) {
            tempIngredients.add(new Ingredient(meal.get("strIngredient3").getAsString(), meal.get("strMeasure3").getAsString()));
        }

        if (meal.get("strIngredient4") != null) {
            tempIngredients.add(new Ingredient(meal.get("strIngredient4").getAsString(), meal.get("strMeasure4").getAsString()));
        }

        if (meal.get("strIngredient5") != null) {
            tempIngredients.add(new Ingredient(meal.get("strIngredient5").getAsString(), meal.get("strMeasure5").getAsString()));
        }

        if (meal.get("strIngredient6") != null) {
            tempIngredients.add(new Ingredient(meal.get("strIngredient6").getAsString(), meal.get("strMeasure6").getAsString()));
        }

        if (meal.get("strIngredient7") != null) {
            tempIngredients.add(new Ingredient(meal.get("strIngredient7").getAsString(), meal.get("strMeasure7").getAsString()));
        }

        if (meal.get("strIngredient8") != null) {
            tempIngredients.add(new Ingredient(meal.get("strIngredient8").getAsString(), meal.get("strMeasure8").getAsString()));
        }

        if (meal.get("strIngredient9") != null) {
            tempIngredients.add(new Ingredient(meal.get("strIngredient9").getAsString(), meal.get("strMeasure9").getAsString()));
        }

        if (meal.get("strIngredient10") != null) {
            tempIngredients.add(new Ingredient(meal.get("strIngredient10").getAsString(), meal.get("strMeasure10").getAsString()));
        }

        if (meal.get("strIngredient11") != null) {
            tempIngredients.add(new Ingredient(meal.get("strIngredient11").getAsString(), meal.get("strMeasure11").getAsString()));
        }

        if (meal.get("strIngredient12") != null) {
            tempIngredients.add(new Ingredient(meal.get("strIngredient12").getAsString(), meal.get("strMeasure12").getAsString()));
        }

        if (meal.get("strIngredient13") != null) {
            tempIngredients.add(new Ingredient(meal.get("strIngredient13").getAsString(), meal.get("strMeasure13").getAsString()));
        }

        if (meal.get("strIngredient14") != null) {
            tempIngredients.add(new Ingredient(meal.get("strIngredient14").getAsString(), meal.get("strMeasure14").getAsString()));
        }

        if (meal.get("strIngredient15") != null) {
            tempIngredients.add(new Ingredient(meal.get("strIngredient15").getAsString(), meal.get("strMeasure15").getAsString()));
        }

        if (meal.get("strIngredient16") != null) {
            tempIngredients.add(new Ingredient(meal.get("strIngredient16").getAsString(), meal.get("strMeasure16").getAsString()));
        }

        if (meal.get("strIngredient17") != null) {
            tempIngredients.add(new Ingredient(meal.get("strIngredient17").getAsString(), meal.get("strMeasure17").getAsString()));
        }

        if (meal.get("strIngredient18") != null) {
            tempIngredients.add(new Ingredient(meal.get("strIngredient18").getAsString(), meal.get("strMeasure18").getAsString()));
        }

        if (meal.get("strIngredient19") != null) {
            tempIngredients.add(new Ingredient(meal.get("strIngredient19").getAsString(), meal.get("strMeasure19").getAsString()));
        }

        if (meal.get("strIngredient20") != null) {
            tempIngredients.add(new Ingredient(meal.get("strIngredient20").getAsString(), meal.get("strMeasure20").getAsString()));
        }

        if (tempIngredients.size() > 0) {
            JsonArray ja = new JsonArray();
            ja.add(new Gson().toJsonTree(tempIngredients));
            meal.add("ingredients", ja);
        }

        return new Gson().fromJson(meal, Meal.class );
    }
}
