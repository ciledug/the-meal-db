package id.my.farmasikita.themealdb;

import android.os.Bundle;
import android.os.Looper;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.my.farmasikita.themealdb.utils.Tools;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.ivLogo)
    ImageView ivLogo;

    @BindView(R.id.tvDisclaimer)
    TextView tvDisclaimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        initScreen();
        setupIntentAnimation();
    }

    private void initScreen() {
        SpannableStringBuilder ssb = new SpannableStringBuilder(getString(R.string.all_writings_and_assets_are_taken_from));
        ssb.append("\n").append(getString(R.string.api_url));
        ssb.setSpan(
            new ForegroundColorSpan(ContextCompat.getColor(getApplicationContext(), R.color.purple_500)),
            ssb.toString().indexOf("\n") + 1,
            ssb.toString().length(),
            SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE
        );
        tvDisclaimer.setText(ssb);
    }

    private void setupIntentAnimation() {
        new Thread() {
            @Override
            public void run() {
                try {
                    super.run();
                    sleep(2000);
                } catch (InterruptedException e) {
                } finally {
                    Looper.prepare();
                    startAnimation();
                    Looper.loop();
                }
            }
        }.start();
    }

    private void startAnimation() {
        new Thread() {
            @Override
            public void run() {
                try {
                    super.run();
                    Animation animUtil = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
                    ivLogo.startAnimation(animUtil);
                    animUtil.cancel();
                    sleep(100);
                } catch (InterruptedException e) {
                } finally {
                    Looper.prepare();
                    gotoMain();
                    Looper.loop();
                }
            }
        }.start();
    }

    private void gotoMain() {
        Tools.gotoMainActivity(this);
    }
}