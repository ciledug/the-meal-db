package id.my.farmasikita.themealdb.ui.meals;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import id.my.farmasikita.themealdb.models.CategoryList;
import id.my.farmasikita.themealdb.repositories.DataRepository;

public class MealsViewModel extends ViewModel {

    private final String TAG = getClass().getSimpleName();

    private DataRepository mDataRepository;
    private MutableLiveData<CategoryList> mCategories;

    public MealsViewModel() {
        mDataRepository = DataRepository.getInstance();
    }

    public LiveData<CategoryList> requestMeals(String category) {
        if (mCategories == null) {
            mCategories = mDataRepository.requestMeals(category);
        }
        return mCategories;
    }
}
