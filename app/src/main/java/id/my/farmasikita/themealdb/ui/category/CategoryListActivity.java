package id.my.farmasikita.themealdb.ui.category;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.my.farmasikita.themealdb.R;
import id.my.farmasikita.themealdb.fragments.ProgressDialogFragment;
import id.my.farmasikita.themealdb.models.CategoryList;
import id.my.farmasikita.themealdb.ui.meals.MealsListActivity;
import id.my.farmasikita.themealdb.utils.AppConstants;

public class CategoryListActivity extends AppCompatActivity {

    private final String TAG = getClass().getSimpleName();

    private CategoryAdapter mAdapter;
    private CategoryViewModel mViewModel;
    private MutableLiveData<CategoryList> mCategoryList = new MutableLiveData<>();

    private ProgressDialogFragment mProgressDialog;

    private CategoryAdapter.CategoryAdapterListener mCategoryAdapterListener = new CategoryAdapter.CategoryAdapterListener() {
        @Override
        public void onCategoryClicked(String category) {
            gotoMealsList(category);
        }
    };

    @BindView(R.id.tbToolbar)
    Toolbar tbToolbar;

    @BindView(R.id.rvCategoryListContainer)
    RecyclerView rvCategoryListContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);
        ButterKnife.bind(this);

        mViewModel = new ViewModelProvider(this).get(CategoryViewModel.class);
        mProgressDialog = ProgressDialogFragment.newInstance();

        initScreen();
        requestCategoryList();
    }

    private void initScreen() {
        setSupportActionBar(tbToolbar);
        tbToolbar.setNavigationIcon(null);
        rvCategoryListContainer.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        );
    }

    private void requestCategoryList() {
        mProgressDialog.show(getSupportFragmentManager(), AppConstants.TAG_PROGRESS);

        mViewModel.requestCategories().observe(this, new Observer<CategoryList>() {
            @Override
            public void onChanged(CategoryList categoryList) {
                if (mAdapter == null) {
                    mAdapter = new CategoryAdapter(getApplicationContext(), mViewModel.requestCategories().getValue(), mCategoryAdapterListener);
                    rvCategoryListContainer.setAdapter(mAdapter);
                }

                mCategoryList.setValue(categoryList);
                mAdapter.addItems(mCategoryList.getValue().getCategories());
                mProgressDialog.dismiss();
            }
        });
    }

    public void gotoMealsList(String category) {
        Bundle b = new Bundle();
        b.putString(AppConstants.EXTRA_SELECTED_CATEGORY, category);

        Intent i = new Intent(this, MealsListActivity.class);
        i.putExtra(AppConstants.EXTRA_BUNDLE, b);
        startActivity(i);
    }
}
