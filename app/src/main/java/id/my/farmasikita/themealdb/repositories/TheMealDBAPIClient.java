package id.my.farmasikita.themealdb.repositories;

import androidx.lifecycle.MutableLiveData;

import id.my.farmasikita.themealdb.models.CategoryList;
import id.my.farmasikita.themealdb.models.Meal;
import id.my.farmasikita.themealdb.utils.TheMealDBRetrofitUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TheMealDBAPIClient {

    private final String TAG = getClass().getSimpleName();

    private static TheMealDBAPIClient apiClient = null;
    private TheMealDBAPIService mApiService = null;
    private MutableLiveData<Meal> mMeal;
    private MutableLiveData<CategoryList> mCategories;

    public TheMealDBAPIClient(TheMealDBAPIService apiService) {
        mApiService = apiService;
    }

    public static TheMealDBAPIClient getInstance() {
        synchronized (TheMealDBAPIClient.class) {
            if (apiClient == null) {
                apiClient = new TheMealDBAPIClient(TheMealDBRetrofitUtil.create());
            }
        }
        return apiClient;
    }

    public MutableLiveData<CategoryList> requestCategories() {
        MutableLiveData<CategoryList> data = new MutableLiveData<>();
        mApiService.requestCategories()
            .enqueue(new Callback<CategoryList>() {
                @Override
                public void onResponse(Call<CategoryList> call, Response<CategoryList> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        data.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<CategoryList> call, Throwable t) {
                    data.setValue(new CategoryList());
                }
            });
        return data;
    }

    public MutableLiveData<CategoryList> requestMeals(String category) {
        MutableLiveData<CategoryList> data = new MutableLiveData<>();
        mApiService.requestMeals(category)
                .enqueue(new Callback<CategoryList>() {
                    @Override
                    public void onResponse(Call<CategoryList> call, Response<CategoryList> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            data.setValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<CategoryList> call, Throwable t) {
                        data.setValue(new CategoryList());
                    }
                });
        return data;
    }

    public MutableLiveData<CategoryList> requestMealDetail(String idMeal) {
        MutableLiveData<CategoryList> data = new MutableLiveData<>();
        mApiService.requestMealDetail(idMeal)
                .enqueue(new Callback<CategoryList>() {
                    @Override
                    public void onResponse(Call<CategoryList> call, Response<CategoryList> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            data.setValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(Call<CategoryList> call, Throwable t) {
                        data.setValue(new CategoryList());
                    }
                });
        return data;
    }
}
