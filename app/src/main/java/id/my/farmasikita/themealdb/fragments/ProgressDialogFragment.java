package id.my.farmasikita.themealdb.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import id.my.farmasikita.themealdb.R;
import id.my.farmasikita.themealdb.utils.Tools;

public class ProgressDialogFragment extends DialogFragment {

    public ProgressDialogFragment() {
    }

    public static ProgressDialogFragment newInstance() {
        return new ProgressDialogFragment();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View dialogLayout = LayoutInflater.from(getContext()).inflate(R.layout.fragment_progress_dialog, null);
        Dialog dialog = new AlertDialog.Builder(getActivity())
                .setCancelable(false)
                .setView(dialogLayout)
                .create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Tools.setImage(getContext(), dialogLayout.findViewById(R.id.ivProgressIcon), getContext().getResources().getDrawable(R.drawable.ic_launcher));
        return dialog;
    }
}