package id.my.farmasikita.themealdb.ui.detail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.my.farmasikita.themealdb.R;
import id.my.farmasikita.themealdb.fragments.ProgressDialogFragment;
import id.my.farmasikita.themealdb.models.CategoryList;
import id.my.farmasikita.themealdb.models.Ingredient;
import id.my.farmasikita.themealdb.models.Meal;
import id.my.farmasikita.themealdb.utils.AppConstants;
import id.my.farmasikita.themealdb.utils.Tools;

public class DetailActivity extends AppCompatActivity {

    private final String TAG = getClass().getSimpleName();

    private DetailViewModel mDetailViewModel;
    private String mSelectedIdMeal = "";
    private ProgressDialogFragment mProgressDialog;

    @BindView(R.id.tbToolbar)
    Toolbar tbToolbar;

    @BindView(R.id.ivMealImage)
    ImageView ivMealImage;

    @BindView(R.id.tvMealName)
    TextView tvMealName;

    @BindView(R.id.tvMealCategory)
    TextView tvMealCategory;

    @BindView(R.id.tvMealArea)
    TextView tvMealArea;

    @BindView(R.id.tvMealInstruction)
    TextView tvMealInstruction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mDetailViewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        mProgressDialog = ProgressDialogFragment.newInstance();
        ButterKnife.bind(this);

        if (getIntent().hasExtra(AppConstants.EXTRA_BUNDLE)) {
            Bundle b = getIntent().getBundleExtra(AppConstants.EXTRA_BUNDLE);
            if (b != null) {
                mSelectedIdMeal = b.getString(AppConstants.EXTRA_SELECTED_MEAL);
            }
        }

        initScreen();
        requestDetail();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initScreen() {
        Tools.setupToolbarNavigation(this, tbToolbar, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void requestDetail() {
        mProgressDialog.show(getSupportFragmentManager(), AppConstants.TAG_PROGRESS);

        mDetailViewModel.requestDetail(mSelectedIdMeal).observe(this, new Observer<CategoryList>() {
            @Override
            public void onChanged(CategoryList categoryList) {
                showDetail(categoryList.getCategories().get(0));
                mProgressDialog.dismiss();
            }
        });
    }

    private void showDetail(Meal meal) {
        if (meal != null) {
            Tools.setImage(this, ivMealImage, meal.getStrMealThumb());
            tvMealArea.setText(meal.getStrArea());
            tvMealCategory.setText(meal.getStrCategory());
            tvMealInstruction.setText(meal.getStrInstructions().replaceAll("\r\n|\r\n\r\n", "\r\n\r\n"));
            tvMealName.setText(meal.getStrMeal());
        }
    }
}