package id.my.farmasikita.themealdb.repositories;

import androidx.lifecycle.MutableLiveData;

import id.my.farmasikita.themealdb.models.CategoryList;

public class DataRepository {

    private final String TAG = getClass().getSimpleName();

    private static DataRepository mDataRepository = null;
    private static TheMealDBAPIClient mApiClient = null;

    private DataRepository() {
        mApiClient = TheMealDBAPIClient.getInstance();
    }

    public static DataRepository getInstance() {
        synchronized (DataRepository.class) {
            if (mDataRepository == null) {
                mDataRepository = new DataRepository();
            }
        }
        return mDataRepository;
    }

    public MutableLiveData<CategoryList> requestCategories() {
        return mApiClient.requestCategories();
    }

    public MutableLiveData<CategoryList> requestMeals(String category) {
        return mApiClient.requestMeals(category);
    }

    public MutableLiveData<CategoryList> requestMealDetail(String idMeal) {
        return mApiClient.requestMealDetail(idMeal);
    }
}
