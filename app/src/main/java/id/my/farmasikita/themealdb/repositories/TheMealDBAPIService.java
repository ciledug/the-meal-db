package id.my.farmasikita.themealdb.repositories;

import id.my.farmasikita.themealdb.models.CategoryList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TheMealDBAPIService {

    String developmentURL = "https://www.themealdb.com/";
    int TIMEOUT = 10;
    int READ_TIMEOUT = 10;

    @GET("api/json/v1/1/list.php?c=list")
    Call<CategoryList> requestCategories();

    @GET("api/json/v1/1/filter.php")
    Call<CategoryList> requestMeals(@Query("c") String category);

    @GET("api/json/v1/1/lookup.php")
    Call<CategoryList> requestMealDetail(@Query("i") String idMeal);
}
